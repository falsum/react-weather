// A lot of actions and creators. Monkeytyping.
// Better to use helper libs

export const ADD_CITY = 'ADD_CITY';
export const REMOVE_CITY = 'REMOVE_CITY';

export const REQUEST_CITY_WEATHER = 'REQUEST_CITY_WEATHER';
export const RECEIVE_CITY_WEATHER = 'RECEIVE_CITY_WEATHER';
export const INVALIDATE_CITY_WEATHER = 'INVALIDATE_CITY_WEATHER';
export const ERROR_CITY_WEATHER = 'ERROR_CITY_WEATHER';

export const weatherTypes = {
    current: 'current',
    fore5: 'fore5',
    fore16: 'fore16'
};
const apiKey = 'c239db06b7956237554131f916a30de8';
const path = {
    [weatherTypes.current]: `http://api.openweathermap.org/data/2.5/weather?APPID=${apiKey}&units=metric&id=`,
    [weatherTypes.fore5]: `http://api.openweathermap.org/data/2.5/forecast?APPID=${apiKey}&units=metric&id=`,
    [weatherTypes.fore16]: `http://api.openweathermap.org/data/2.5/forecast/daily?APPID=${apiKey}&units=metric&cnt=16&id=`
}

export const addCity = (id, name) => ({
    type: ADD_CITY,
    id,
    name
});

export const removeCity = id => ({
    type: REMOVE_CITY,
    id
});

export const requestCityWeather = (id, weatherType) => ({
    type: REQUEST_CITY_WEATHER,
    id,
    weatherType
});

export const receiveCityWeather = (id, weatherType, data) => ({
    type: RECEIVE_CITY_WEATHER,
    id,
    weatherType,
    data
});

export const invalidateCityWeather = id => ({
    type: INVALIDATE_CITY_WEATHER,
    id
});

export const errorCityWeather = (id, weatherType) => ({
    type: ERROR_CITY_WEATHER,
    id, 
    weatherType
});

const fetchCityWeather = (id, weatherType) => {
    return dispatch => {
        dispatch(requestCityWeather(id, weatherType));
        return fetch(path[weatherType] + id)
            .then(response => response.json())
            .then(data => {
                dispatch(receiveCityWeather(id, weatherType, data))
            })
            .catch(() => dispatch(errorCityWeather(id, weatherType)));
    };
};

const shouldFetchCityWeather = (state, id, weatherType) => {
    const weather = state.cityList[id][weatherType];
    if (!weather) {
        return true;
    }
    if (weather.isFetching) {
        return false;
    }
    return weather.didInvalidate;
};

export const ensureCityWeather = (id, weatherType) => {
    return (dispatch, getState) => {
        if (shouldFetchCityWeather(getState(), id, weatherType)) {
            return dispatch(fetchCityWeather(id, weatherType));
        }
        return Promise.resolve();
    };
};
