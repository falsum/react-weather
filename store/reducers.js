import { combineReducers } from 'redux';
import {
    weatherTypes,
    ADD_CITY,
    REMOVE_CITY,
    REQUEST_CITY_WEATHER,
    RECEIVE_CITY_WEATHER,
    INVALIDATE_CITY_WEATHER,
    ERROR_CITY_WEATHER
} from './actions';

const weather = (state = {}, action) => {
    switch (action.type) {
        case REQUEST_CITY_WEATHER:
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false,
                isError: false
            });
        case RECEIVE_CITY_WEATHER:
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                data: action.data,
                isError: false
            });
        case INVALIDATE_CITY_WEATHER:
            return Object.assign({}, state, {
                didInvalidate: true,
                isError: false
            });
        case ERROR_CITY_WEATHER:
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                isError: true
            });
        default:
            return state;
    }
};

const city = (state = {}, action) => {
    switch (action.type) {
        case ADD_CITY:
            return {
                id: action.id,
                name: action.name
            };
        case REQUEST_CITY_WEATHER:
        case RECEIVE_CITY_WEATHER:
        case ERROR_CITY_WEATHER:
            return Object.assign({}, state, {
                [action.weatherType]: weather(state[action.weatherType], action)
            })
        case INVALIDATE_CITY_WEATHER:
            return Object.assign({}, state, {
                [weatherTypes.current]: weather(state[weatherTypes.current], action),
                [weatherTypes.fore5]: weather(state[weatherTypes.fore5], action),
                [weatherTypes.fore16]: weather(state[weatherTypes.fore16], action)
            })
        default:
            return state;
    }
};

const cityList = (state = {}, action) => {
    switch (action.type) {
        case REMOVE_CITY:
            return Object.keys(state).filter(key => key !== action.id).reduce((result, item) => {
                result[item] = state[item];
                return result;
            }, {});
        case ADD_CITY:
        case REQUEST_CITY_WEATHER:
        case RECEIVE_CITY_WEATHER:
        case INVALIDATE_CITY_WEATHER:
        case ERROR_CITY_WEATHER:
            return Object.assign({}, state, {
                [action.id]: city(state[action.id], action)
            });
        default:
            return state;
    }
};

const rootReducer = combineReducers({
    cityList
});

export default rootReducer;