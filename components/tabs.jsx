import React from 'react';

function NavTab(props) {
    const { isActive, onTabClick, text } = props;
    const className = isActive ? 'active' : '';
    return (
        <li className={className}>
            <a onClick={onTabClick}>{text}</a>
        </li>
    );
}

export function Tab(props) {
    return (
        <div className="tab-pane active">
            {props.children}
        </div>
    );
}

export class TabList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedIdx: props.selectedIdx || 0
        }
    }

    onTabClick(selectedIdx) {
        this.setState(state => Object.assign({}, state, { selectedIdx }));
    }

    render() {
        const { selectedIdx } = this.state;
        const { children } = this.props;
        return (
            <div>
                <ul className="nav nav-tabs">
                    {children.map((child, idx) =>
                        <NavTab key={idx} text={child.props.title} isActive={selectedIdx === idx} onTabClick={() => this.onTabClick(idx)} />
                    )}
                </ul>
                <div className="tab-content">
                    {children[selectedIdx]}
                </div>
            </div>
        );
    }
}