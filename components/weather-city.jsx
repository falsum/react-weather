import React from 'react';
import { connect } from 'react-redux';

import { weatherTypes, invalidateCityWeather, removeCity } from './../store/actions';

import WeatherType from './weather-type';
import { TabList, Tab } from './tabs';

const WeatherCity = props => {
    const { cityId, cityName, invalidateCity, removeCity } = props;

    return (
        <div className="col-md-6">
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title">{cityName}</h3>
                </div>
                <div className="panel-body">
                    <TabList>
                        <Tab title="Now">
                            <WeatherType type={weatherTypes.current} cityId={cityId} />
                        </Tab>
                        <Tab title="5 Days">
                            <WeatherType type={weatherTypes.fore5} cityId={cityId} />
                        </Tab>
                        <Tab title="16 Days">
                            <WeatherType type={weatherTypes.fore16} cityId={cityId} />
                        </Tab>
                    </TabList>
                </div>
                <div className="panel-footer">
                    <button type="button" className="btn btn-danger btn-sm" onClick={removeCity}>
                        <span className="glyphicon glyphicon-remove"></span>
                    </button>
                    <button type="button" className="btn btn-success btn-sm pull-right" onClick={invalidateCity}>
                        <span className="glyphicon glyphicon-refresh"></span>
                    </button>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state, props) => {
    const city = state.cityList[props.cityId];
    return {
        cityId: city.id,
        cityName: city.name
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        invalidateCity: () => dispatch(invalidateCityWeather(props.cityId)),
        removeCity: () => dispatch(removeCity(props.cityId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(WeatherCity);