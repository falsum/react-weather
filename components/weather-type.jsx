import React from 'react';
import { connect } from 'react-redux';

import { weatherTypes } from './../store/actions';
import { ensureCityWeather } from './../store/actions';

import { WeatherCurrent, WeatherFore5, WeatherFore16 } from './weather-controls';

class WeatherType extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.ensureCityWeather();
    }

    componentDidUpdate(prevProps) {
        if (this.props.type !== prevProps.type || this.props.didInvalidate !== prevProps.didInvalidate) {
            this.props.ensureCityWeather();
        }
    }

    renderData() {
        const { type, data } = this.props;
        if (!data) {
            return null;
        }
        switch (type) {
            case weatherTypes.current:
                return (<WeatherCurrent data={data} />)
            case weatherTypes.fore5:
                return (<WeatherFore5 data={data} />)
            case weatherTypes.fore16:
                return (<WeatherFore16 data={data} />)
            default:
                return null;
        }
    }

    renderOverlay() {
        return (
            <div className="weather-type-overlay">
                <h4 className="text-warning">Loading</h4>
            </div>
        );
    }

    renderError() {
        return (
            <div className="weather-type-overlay">
                <h4 className="text-danger">Loading Error. Use refresh</h4>
            </div>
        );
    }

    render() {
        const { isFetching, isError } = this.props;
        return (
            <div className="weather-type">
                {isError ? this.renderError() : isFetching ? this.renderOverlay() : this.renderData()}
            </div>
        );
    }
};

const mapStateToProps = (state, props) => {
    const weatherType = state.cityList[props.cityId][props.type];
    return !weatherType ? {
        isFetching: true,
        didInvalidate: false,
        isError: false
    } : {
            isFetching: weatherType.isFetching,
            didInvalidate: weatherType.didInvalidate,
            isError: weatherType.isError,
            data: weatherType.data
        };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        ensureCityWeather: () => dispatch(ensureCityWeather(props.cityId, props.type))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(WeatherType);