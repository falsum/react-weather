import React from 'react';

const NavBar = props => {
    return (
        <nav className="navbar navbar-default navbar-static-top">
            <div className="container">
                <div className="navbar-header">
                    <a className="navbar-brand" href="">react-weather</a>
                </div>
                <button type="button" className="btn btn-success navbar-btn navbar-right" title="Add City" onClick={props.onAddClick}>
                    <span className="glyphicon glyphicon-plus"></span>
                </button>
            </div>
        </nav>
    );
};

export default NavBar;