import React from 'react';

let openModalCount = 0;

export default class Modal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: props.isOpen
        }

        this.close = this.close.bind(this);
        this.onVisibilityChange = props.onVisibilityChange;
        this.onBackdropClose = this.onBackdropClose.bind(this);
    }

    handleHtmlBody(isOpen) {
        if (isOpen) {
            openModalCount++;
        } else {
            openModalCount--;
        }
        const bodyClassList = document.getElementsByTagName('body')[0].classList;
        const bodyClassName = 'modal-open';
        if (openModalCount > 0) {
            bodyClassList.add(bodyClassName);
        } else {
            bodyClassList.remove(bodyClassName);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.isOpen !== this.props.isOpen) {
            this.setState(state => {
                this.handleHtmlBody(this.props.isOpen);
                return Object.assign({}, state, { isOpen: this.props.isOpen });
            });
            return;
        }
    }

    close() {
        this.onVisibilityChange(false)
    }

    onBackdropClose(event) {
        if (event.currentTarget === event.target) {
            this.close();
        }
    }

    render() {
        const isOpen = this.state.isOpen;

        if (!isOpen) {
            return null;
        }

        const { title, children, onSubmit } = this.props;

        return (
            <div>
                <div className="modal fade in" style={{ display: 'block' }} onClick={this.onBackdropClose}>
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" onClick={this.close}><span>&times;</span></button>
                                <h4 className="modal-title">{title}</h4>
                            </div>
                            <div className="modal-body">
                                {children}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" onClick={this.close}>
                                    <span className="glyphicon glyphicon-remove"></span>
                                    {' '}
                                    Close
                                </button>
                                <button type="button" className="btn btn-primary" onClick={onSubmit}>
                                    <span className="glyphicon glyphicon-ok"></span>
                                    {' '}
                                    Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop fade in"></div>
            </div>
        );
    }
}