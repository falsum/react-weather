import React from 'react';
import { connect } from 'react-redux';

import {addCity} from './../store/actions';

import WeatherFeed from './weather-feed';
import NavBar from './nav-bar';
import Modal from './modal';
import Typeahead from './typeahead';

import cityOptions from './../resources/city-options';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            addModalVisible: false,
            cityError: null
        };

        this.onAddClick = this.onAddClick.bind(this);
        this.onModalSubmit = this.onModalSubmit.bind(this);
        this.onModalVisibilityChange = this.onModalVisibilityChange.bind(this);
    }

    onAddClick() {
        this.setState(state => Object.assign({}, state, { addModalVisible: true }));
    }

    onModalSubmit() {
        const selectedCityOption = this.typeahead && this.typeahead.getSelected();
        if (!selectedCityOption) {
            this.setState(state => Object.assign({}, state, {
                cityError: 'City not selected'
            }));
            return;
        }

        const { dispatch } = this.props;
        dispatch(addCity(selectedCityOption.value, selectedCityOption.text));
        this.setState(state => Object.assign({}, state, {
            cityError: null,
            addModalVisible: false
        }));
    }

    onModalVisibilityChange(isOpen) {
        this.setState(state => Object.assign({}, state, { addModalVisible: isOpen }));
    }

    render() {
        const { addModalVisible, cityError } = this.state;
        const formGroupClassName = 'form-group' + (cityError ? ' has-error' : '');

        return (
            <div id="app">
                <NavBar onAddClick={this.onAddClick} />
                <Modal
                    isOpen={addModalVisible}
                    title="Add City"
                    onVisibilityChange={this.onModalVisibilityChange}
                    onSubmit={this.onModalSubmit}>
                    <div className={formGroupClassName}>
                        <Typeahead list={cityOptions} placeholder="City Name" ref={ref => this.typeahead = ref} />
                        {cityError && <div className="help-block">{cityError}</div>}
                    </div>
                </Modal>
                <section id="content">
                    <div className="container">
                        <WeatherFeed onAddClick={this.onAddClick} />
                    </div>
                </section>
                <footer>
                    <div className="container">
                        Copy 2017
                    </div>
                </footer>
            </div>
        );
    }
}

export default connect()(App);