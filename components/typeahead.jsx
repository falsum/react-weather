import React from 'react';
import ReactDOM from 'react-dom';

export default class Typeahead extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: '',
            filteredList: [],
            selectedItem: null,
            activeItem: null,
            listVisible: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.onFocus = this.onFocus.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);

        /* using fat arrow to bind to instance not prototype */
        this.handleDocumentClick = (event) => {
            const element = ReactDOM.findDOMNode(this.element);

            if (!element.contains(event.target)) {
                this.setState(state => Object.assign({}, state, { listVisible: false }));
            }
        }
    }

    componentDidMount() {
        window.document.addEventListener('click', this.handleDocumentClick);
    }

    componentWillUnmount() {
        window.document.removeEventListener('click', this.handleDocumentClick)
    }

    getSelected() {
        return this.state.selectedItem;
    }

    handleClick(item) {
        this.setState(state => Object.assign({}, state, {
            value: item.text,
            selectedItem: item,
            activeItem: null,
            filteredList: this.getFilteredList(item.text),
            listVisible: false
        }));
        //this.props.onSelect && this.props.onSelect(item);
    }

    getFilteredList(value) {
        const { list } = this.props;
        const valueLower = value.toLowerCase();
        let counter = 0;
        return !value ? [] : list.filter(item => {
            if (counter > 9) {
                return false;
            }
            const textLower = item.text.toLowerCase();
            if (textLower.indexOf(valueLower) === 0) {
                counter++;
                return true;
            }
        });
    }

    handleChange(event) {
        const value = event.target.value;
        const valueLower = value.toLowerCase();
        const filteredList = this.getFilteredList(value);

        this.setState(state => Object.assign({}, state, {
            value,
            filteredList
        }));
    }

    onFocus() {
        this.setState(state => Object.assign({}, state, { listVisible: true }));
    }

    onKeyDown(event) {
        const { filteredList, activeItem } = this.state;

        if (!filteredList.length) {
            this.setState(state => Object.assign({}, state, { activeItem: null }));
            return;
        }

        const key = event.key;
        if (key === 'Enter' && activeItem) {
            this.setState(state => Object.assign({}, state, {
                value: activeItem.text,
                filteredList: [],
                listVisible: false,
                selectedItem: activeItem,
                activeItem: null
            }));
            //this.props.onSelect && this.props.onSelect(value);
            event.preventDefault();
            return;
        }

        if (key !== 'ArrowUp' && key !== 'ArrowDown') {
            return;
        }
        event.preventDefault();

        let activeIdx = !activeItem ? -1 : filteredList.indexOf(activeItem);
        if (key === 'ArrowDown') {
            activeIdx++;
            if (activeIdx >= filteredList.length) {
                activeIdx = 0;
            }
        }
        if (key === 'ArrowUp') {
            activeIdx--;
            if (activeIdx < 0) {
                activeIdx = filteredList.length - 1;
            }
        }
        this.setState(state => Object.assign({}, state, { activeItem: filteredList[activeIdx] }));
    }

    renderDropDown() {
        const { listVisible, filteredList, value, activeItem } = this.state;
        if (!listVisible || !filteredList.length || !value.length) {
            return null;
        }

        return (
            <div className="dropdown open">
                <ul className="dropdown-menu">
                    {filteredList.map(item =>
                        <li className={activeItem === item ? 'active' : null} key={item.value}>
                            <a onClick={() => this.handleClick(item)}>{item.text}</a>
                        </li>
                    )}
                </ul>
            </div>
        );
    }

    render() {
        const { value } = this.state;
        const { placeholder } = this.props;

        return (
            <div className="typeahead" ref={element => this.element = element}>
                <input type="text" className="form-control" placeholder={placeholder} value={value} onChange={this.handleChange} onFocus={this.onFocus} onKeyDown={this.onKeyDown} />
                {this.renderDropDown()}
            </div>
        );
    }
}