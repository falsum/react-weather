import React from 'react';
import { connect } from 'react-redux';

import WeatherCity from './weather-city';

class WeatherFeed extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { cityIdList, onAddClick } = this.props;
        if (!cityIdList || !cityIdList.length) {
            return <p className="text-center">
                No cities. Click {' '}
                <button type="button" className="btn btn-success" title="Add City" onClick={onAddClick}>
                    <span className="glyphicon glyphicon-plus"></span>
                </button>
            </p>
        }
        return (
            <div className="row">
                {cityIdList.map(cityId =>
                    <WeatherCity key={cityId} cityId={cityId} />
                )}
            </div>
        );
    }
}

const mapStateToProps = state => {
    const cityIdList = Object.keys(state.cityList);
    return { cityIdList };
};

export default connect(mapStateToProps)(WeatherFeed);