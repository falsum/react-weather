import React from 'react';

const WeatherIcon = props => {
    const { iconId, small } = props;
    const iconHref = `url(http://openweathermap.org/img/w/${iconId}.png)`;
    const className = 'weather-icon' + (small ? ' weather-icon-sm' : '');
    return (
        <span className={className} style={{ backgroundImage: iconHref }} />
    );

};

export const WeatherCurrent = props => {
    const { data } = props;

    if (!data) {
        return null;
    }

    const iconIds = [...new Set(data.weather.map(w => w.icon))]; // remove duplicates
    const text = data.weather.map(w => w.description).join(', ');
    return (
        <div className="weather-type-content">
            {data.main.temp}
            {' '}
            {iconIds.map(id =>
                <WeatherIcon key={id} iconId={id} />
            )}
            {' '}
            {text}
        </div>
    );
};

export const WeatherFore5 = props => {
    const { data } = props;

    if (!data) {
        return null;
    }

    const mapped = data.list.map(item => {
        const date = item.dt_txt.split(' ');
        return {
            day: date[0],
            time: date[1].substr(0, 5),
            temp: item.main.temp,
            iconIds: [...new Set(item.weather.map(w => w.icon))] // remove duplicates
        }
    });

    const distinctTimes = [...new Set(mapped.map(item => item.time))].sort();
    const distinctDays = [...new Set(mapped.map(item => item.day))].sort();

    // convert to table
    const days = distinctDays.map(day =>
        distinctTimes.map(time =>
            mapped.find(item => item.day === day && item.time === time)));

    return (
        <div className="weather-type-content">
            <table className="weather-table">
                <thead>
                    <tr>
                        <th></th>
                        {distinctTimes.map(t =>
                            <th key={t}>{t}</th>
                        )}
                    </tr>
                </thead>
                <tbody>
                    {distinctDays.map((d, idx) =>
                        <tr key={d}>
                            <th>{d}</th>
                            {days[idx].map((day, idx) =>
                                <td key={distinctTimes[idx]}>
                                    {day &&
                                        <span>
                                            {day.temp}
                                            <br />
                                            {day.iconIds.map(id =>
                                                <WeatherIcon small={true} key={id} iconId={id} />
                                            )}
                                        </span>
                                    }
                                </td>
                            )}
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    );
};

const WeatherFore16Table = props => {
    const { data } = props;

    const mapped = data.map(item => ({
        key: item.dt,
        title: new Date(item.dt * 1000).toLocaleDateString(),
        min: item.temp.min,
        max: item.temp.max,
        iconIds: [...new Set(item.weather.map(w => w.icon))] // remove duplicates

    }))

    return (
        <table className="weather-table">
            <thead>
                <tr>
                    {mapped.map(item =>
                        <th key={item.key}>{item.title}</th>
                    )}
                </tr>
            </thead>
            <tbody>
                <tr>
                    {mapped.map(item =>
                        <td key={item.key}>
                            {item.min}
                            <br />
                            {item.max}
                            <br />
                            {item.iconIds.map(id =>
                                <WeatherIcon small={true} key={id} iconId={id} />
                            )}
                        </td>
                    )}
                </tr>
            </tbody>
        </table>
    )
};

export const WeatherFore16 = props => {
    const { data } = props;

    if (!data) {
        return null;
    }

    const data1 = data.list.slice(0, 8);
    const data2 = data.list.slice(8);

    return (
        <div className="weather-type-content">
            <WeatherFore16Table data={data1} />
            <br />
            <WeatherFore16Table data={data2} />
        </div>
    );
};
