var gulp = require('gulp');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');

var gulpLess = require('gulp-less');

const getTaskJs = isProd => {
    return browserify({ entries: './index.jsx', extensions: ['.jsx'], debug: !isProd })
        .transform('babelify', { presets: ['es2015', 'react'] })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('dist'));
};

gulp.task('js.dev', () => getTaskJs(false));
gulp.task('js.prod', () => getTaskJs(true));

gulp.task('less', () => {
    return gulp.src('./less/site.less')
        .pipe(gulpLess())
        .pipe(gulp.dest('dist'));
});

gulp.task('fonts', () => {
    return gulp.src([
        'node_modules/bootstrap/fonts/glyphicons-halflings-regular.*'])
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', ['js.dev', 'less', 'fonts'], () => {
    const exclude = ['!./node_modules/**/*', '!./dist/**/*'];
    gulp.watch(['./**/*.jsx', ...exclude], ['js.dev']);
    gulp.watch(['./**/*.less', ...exclude], ['less']);
});

gulp.task('default', ['js.prod', 'less', 'fonts']);