import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/store';

import App from './components/app';

import cityOptions from './resources/city-options';

const storageKey = 'react-weather';
const preloadedState = { cityList: {} };
try {
    const storageData = localStorage.getItem(storageKey);
    if (storageData) {
        preloadedState.cityList = JSON.parse(storageData);
    }
} catch (e) { }

const store = configureStore(preloadedState);
store.subscribe(() => {
    const cityList = store.getState().cityList;
    const dataToSave = Object.keys(cityList).reduce((result, item) => {
        result[item] = {
            id: cityList[item].id,
            name: cityList[item].name
        };
        return result;
    }, {});
    localStorage.setItem(storageKey, JSON.stringify(dataToSave));
})

const Root = () => {
    return (
        <Provider store={store}>
            <App />
        </Provider>
    );
};

ReactDOM.render(
    <Root />,
    document.getElementById('root')
);