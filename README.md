# README #

Test task. React-table. Demo [http://react-weather.bitballoon.com/](http://react-weather.bitballoon.com/)

### What is this repository for? ###

* Test task

### How do I get set up? ###

* npm install
* cmd gulp
* open index.html

### External Dependencies ###

* Some build tools
* React, Redux. Others are handmade

### Browser support ###
* Chrome is ok, others fail. No polyfill used!!!